"""
Module for actually executing the jobs in different ways.

The execution of the jobs is split into several parts:
- The JobRunner determines how Jobs are scheduled, executed, and when and how they are checked for completion
- The JobContainerClient determines how to interact with the actual containers, e.g. using Docker or Kubernetes

This entire module has seen several major Refactorings. Instead of immediately starting a new Thread for each Job,
handling the entire execution of the Job, new Jobs are put into an asyncio.Queue, and a number of concurrent worker
tasks (as many as there are concurrent jobs allowed) get jobs from that queue, start the container, and await its
completion. If the JES is restarted, any unfinished Jobs are also put back into that Queue.

The ContainerClient part has been moved to a separate module, as well as the Marketplace Service Client.
This module is concerned with different ways of scheduling the containers.
"""

import asyncio
import time
from typing import List, Tuple

from .container_clients import JobContainerClient
from .database import ExecutionDbClient, NewStatus
from .model import ExecStatus, Execution, JobContainer, JobMode
from .mps_client import MarketplaceClient
from .singletons import logger, settings


class JobRunner:
    """This class encapsulates the different methods for scheduling, checking, running and finishing Jobs.
    This class has been refactored to use Python asyncio API. It is no longer using threads and there are
    no longer separate version for a "looping" or "evented" style of execution. The current implementation
    more or less corresponds to the old "evented" variant, but using asyncio instead of threading.
    """

    def __init__(self, client: JobContainerClient, db_client: ExecutionDbClient, mps_client: MarketplaceClient):
        self.client = client
        self.db_client = db_client
        self.mps_client = mps_client

        # multiple queues for each combination of mode and job-class
        # NOTE: The order of keys is relevant for the _enqueue_job method!
        self.mode_type_workers = {
            "STANDALONE-GPU": parse_worker_spec(settings.worker_standalone_gpu),
            "STANDALONE-CPU": parse_worker_spec(settings.worker_standalone_cpu),
            "STANDALONE-PROXY": parse_worker_spec(settings.worker_standalone_proxy),
            "PREPROCESSING-GPU": parse_worker_spec(settings.worker_preprocessing_gpu),
            "PREPROCESSING-CPU": parse_worker_spec(settings.worker_preprocessing_cpu),
            "PREPROCESSING-PROXY": parse_worker_spec(settings.worker_preprocessing_proxy),
        }
        self.mode_type_queues = {mode: asyncio.Queue() for mode in self.mode_type_workers}

    async def on_startup(self):
        """Things to do when the JES starts, in particular checking for Jobs that were running when the JES stopped.
        After JES startup, check jobs that were "RUNNING" when the JES last stopped (but might be finished now)
        or that are still "SCHEDULED" (but might have been cancelled) and put them back into the queue.
        """
        # start asynchronous tasks working on the job queue
        for mode, spec in self.mode_type_workers.items():
            for i, (host, gpu, device_ids) in enumerate(spec, start=1):
                asyncio.create_task(self._worker(i, mode, host, gpu, device_ids))

        asyncio.create_task(self._clean_up_worker())

        # fill job queues with already running or scheduled Jobs
        for job_ex in self.db_client.get_executions_with_status(ExecStatus.RUNNING):
            logger.info("Awaiting Execution for %s after startup", job_ex.job_id)
            try:
                await self._enqueue_job(job_ex)
            except Exception as e:
                logger.warning("Failed to resume job %s: %r", job_ex.job_id, e)

        for job_ex in self.db_client.get_executions_with_status(ExecStatus.SCHEDULED):
            logger.info("Scheduling Execution for %s after startup", job_ex.job_id)
            try:
                await self._enqueue_job(job_ex)
            except Exception as e:
                logger.warning("Failed to schedule job %s: %r", job_ex.job_id, e)

    async def schedule_job(self, job_ex: Execution):
        """Used to schedule a new Job for execution, by adding it to the DB and the Queue."""
        logger.info("Scheduled Job %s", job_ex.job_id)
        self.db_client.insert_execution(job_ex)
        await self._enqueue_job(job_ex)

    async def stop_job(self, job_ex: Execution) -> bool:
        """Stop Job from being executed, either by cancelling a scheduled job, or by killing an already running job.
        Return True if the job was actually cancelled or False if it already completed otherwise.
        """
        if job_ex.status == ExecStatus.SCHEDULED:
            logger.info("Cancelling Job %s", job_ex.job_id)
            # set status -> to be checked when Execution is taken out of the queue
            self._set_status(job_ex.job_id, NewStatus(ExecStatus.STOPPED, None, "Job stopped by user before start"))
            return True
        if job_ex.status == ExecStatus.RUNNING:
            logger.info("Aborting Job %s", job_ex.job_id)
            # set status, kill container -> final status will be set in _await_execution
            self._set_status(job_ex.job_id, NewStatus(ExecStatus.STOPPED, None, "Job stopped by user"))
            job_cont = self.db_client.get_container(job_ex.job_id)
            return await self.client.kill_job(job_cont)
        return False

    async def _worker(self, worker_number: int, mode: str, host: str, use_gpu: bool, device_ids: str):
        """Worker Coroutine running endlessly, getting the job from the queue and executing it.
        Jobs on the queue can be (a) SCHEDULED if they were just posted, or after re-start, or
        (b) RUNNING after re-start, or (c) FAILED if they have been cancelled before start.
        """
        queue = self.mode_type_queues[mode]
        while True:
            job_id = await queue.get()
            logger.info("Worker %s-%d working on Job %s (%d more in queue)", mode, worker_number, job_id, queue.qsize())
            job_ex = self.db_client.get_execution(job_id)
            try:
                if job_ex.status == ExecStatus.SCHEDULED:
                    await self._start_execution(job_ex, host, use_gpu, device_ids)
                elif job_ex.status == ExecStatus.RUNNING:
                    await self._await_execution(job_ex)
                else:
                    logger.info("Job %s was cancelled, skipping", job_id)
            except Exception as e:
                logger.error("Unexpected Error when executing Job %s: %s", job_ex.job_id, e)
                self._set_status(job_ex.job_id, NewStatus(ExecStatus.ERROR, None, str(e)))

    async def _clean_up_worker(self):
        """Worker Coroutine running endlessly, checking for old completed jobs
        which are cleaned up (container/pod is deleted)."""
        if settings.time_to_clean_up_jobs_after_completion >= 0:
            while True:
                logger.info("Checking for old executions...")
                for job_cont in self.db_client.get_old_containers(settings.time_to_clean_up_jobs_after_completion):
                    logger.info("Cleaning up Container for old Execution: %s", job_cont.container_id)
                    await self.client.clean_up(job_cont)
                    self.db_client.set_removed_flag(job_cont.job_id, value=True)
                await asyncio.sleep(
                    max(60, min(15 * 60, settings.time_to_clean_up_jobs_after_completion))
                )  # check every 15 minutes (or shorter), but at least every 1 minute

    async def _start_execution(self, job_ex: Execution, host: str, use_gpu: bool, device_ids: str):
        """Start the job container, update the status in the database, and await its completion."""
        # get App image (existence and access already tested in main.py)
        image_name = await self.mps_client.get_app_image_name(job_ex.app_id, job_ex.organization_id)
        # start execution; possible error when starting the container are handled by worker
        container_id = await self.client.execute_job(job_ex, image_name, host, use_gpu, device_ids)
        logger.info("Running Job %s: %s", job_ex.job_id, container_id)
        self._set_status(job_ex.job_id, NewStatus(ExecStatus.RUNNING, None, None))
        self.db_client.insert_container(JobContainer(job_id=job_ex.job_id, container_id=container_id))
        # update Execution from DB, then await completion (alternatively, just put back into queue?)
        job_ex = self.db_client.get_execution(job_ex.job_id)
        await self._await_execution(job_ex)

    async def _await_execution(self, job_ex: Execution):
        """Wait for container to terminate, handle different cases (completed/failed/error/timeout),
        and update status in DB.
        """
        job_container = self.db_client.get_container(job_ex.job_id)

        # calculate remaining timeout, if any
        timeout = None if job_ex.timeout < 0 else job_ex.timeout - int(time.time() - job_ex.started_at)

        # await job and write result to database
        job_result = await self.client.await_job(job_container, timeout)
        if job_result is not None:
            self._set_status(job_ex.job_id, job_result)
            logger.info("Finished Job %s: %s", job_ex.job_id, job_result)

    def _set_status(self, job_id: str, new_status: NewStatus):
        """Just a shorthand for the logic moved to the DB Client"""
        self.db_client.update_execution_status(job_id, new_status)

    async def _enqueue_job(self, job_ex: Execution):
        """Determine correct Queue for the given Job and add it to that Queue. Raise exception in case no matching
        queue is found or that queue has no workers. In case no app-type is given, enqueue in the first available
        queue for the given job-mode."""
        the_mode = job_ex.mode
        if the_mode == JobMode.POSTPROCESSING:
            the_mode = JobMode.STANDALONE
        if the_mode == JobMode.REPORT:
            raise RuntimeError("JES does not support REPORT Jobs")

        app_req = await self.mps_client.get_app_requirements(job_ex.app_id, job_ex.organization_id, job_ex.mode)

        mode_type = None
        if app_req and app_req.type:
            mode_type = f"{the_mode}-{app_req.type}"
        else:
            logger.warning("Undefined app requirements.")
            if self.mode_type_workers["STANDALONE-GPU"]:
                mode_type = "STANDALONE-GPU"
            elif self.mode_type_workers["STANDALONE-CPU"]:
                mode_type = "STANDALONE-CPU"

        if mode_type not in self.mode_type_workers:
            raise RuntimeError(f"No matching Queue for Job Mode '{the_mode}'")

        if not self.mode_type_workers[mode_type]:
            raise RuntimeError(f"No Workers in Queue '{mode_type}'")

        queue = self.mode_type_queues[mode_type]
        logger.info("Scheduling Job for Queue %s (%d already in queue)", mode_type, queue.qsize())
        await queue.put(job_ex.job_id)


def parse_worker_spec(worker_spec: str) -> List[Tuple[str, bool, str]]:
    """Parse a worked-spec, specifying the available workers for one of the several different queues.
    Each worker can run on a different Docker host, use GPU or not, and have access to some or all
    GPU devices. Specifications for different worker (groups) are separated with "|" (pipe), and the
    format for a single worker or group is "host;spec", where spec is one of either:
    - for GPU Workers, a comma-separated list of GPU Device-ID, e.g. "1,2,3", or empty for "all GPUs"
    - for non-GPU Workers, an "x" followed by the number of workers, e.g. "x4"
    The "host" part can also be empty, meaning the workers will run on the default host (e.g. local docker).

    :param worker_spec: A worker-spec in the above format
    :return: list of tuples in form (host, use_gpu?, device-ids)
    """
    # TODO check syntax using regex or similar, proper error if syntax is incorrect
    result = []
    for part in filter(None, map(str.strip, worker_spec.split("|"))):
        host, spec = part.split(";")
        if "x" in spec:
            result.extend((host, False, "") for _ in range(int(spec[1:])))
        else:
            result.append((host, True, spec))
    return result
