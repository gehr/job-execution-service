"""
Module encapsulating access to the Marketplace Service.
"""

import json
from typing import Optional

from aiohttp.client import ClientConnectionError
from fastapi import HTTPException

from .auth import AioHttpClient
from .model import AppRequirements
from .singletons import logger, settings


class MarketplaceClient:
    """Client encapsulating methods for accessing the Marketplace Service.
    Currently, that's only one method, but more might come later...
    """

    def __init__(self, sender_auth: AioHttpClient):
        self.client = sender_auth
        self.cache_image_name = {}  # (app_id, org_id) -> image_name
        self.cache_app_req = {}  # (app_id, app_mode) -> app_req
        with open(settings.app_requirements_file, encoding="utf-8") as file:
            self.all_app_reqs = json.load(file)

    async def get_app_image_name(self, app_id: str, org_id: str) -> str:
        """Get the Docker image name for the given App ID from the Marketplace Service.
        - Not found or authenticated --> FastAPI HTTP Error
        - Service not found --> aiohttp ClientConnectorError
        """
        if (app_id, org_id) in self.cache_image_name:
            return self.cache_image_name[(app_id, org_id)]

        try:
            app = await self._get_app_info(app_id, org_id)
            image_name = app["registry_image_url"]
            self.cache_image_name[(app_id, org_id)] = image_name
            return image_name
        except (HTTPException, ClientConnectionError) as e:
            logger.error("Error fetching app from marketplace: %s", str(e))
            raise

    async def _get_app_info(self, app_id: str, org_id: str) -> dict:
        if settings.is_compute_provider:
            route = f"{settings.mps_url}/v1/compute/organizations/{org_id}/apps/{app_id}"
            headers = None
        else:
            route = f"{settings.mps_url}/v1/customer/apps/{app_id}"
            headers = {"organization-id": org_id}
        return await self.client.get(route, headers=headers)

    async def get_app_requirements(self, app_id: str, org_id: str, app_mode: str) -> Optional[AppRequirements]:
        """Get the App requirements for the given App ID from the Marketplace Service."""
        if (app_id, app_mode) in self.cache_app_req:
            return self.cache_app_req[(app_id, app_mode)]

        app = await self._get_app_info(app_id, org_id)
        namespace = app["ead"]["namespace"]

        # TODO for now, get from requirements.json in the Dockerimage; but later get requirements from EAD
        if namespace in self.all_app_reqs and app_mode in self.all_app_reqs[namespace]:
            app_req = AppRequirements(**self.all_app_reqs[namespace][app_mode])
        else:
            logger.warning("Requirements for App ID %s in mode %s not found", app_id, app_mode)
            app_req = None

        self.cache_app_req[(app_id, app_mode)] = app_req
        return app_req
