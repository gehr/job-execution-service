from importlib.metadata import version

__version__ = version("job-execution-service")
