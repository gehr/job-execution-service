import logging

from .settings import Settings

settings = Settings()
if settings.rewrite_url_in_wellknown == "":  # docker-compose sets this to blank string if var does not exist
    settings.rewrite_url_in_wellknown = None

logger = logging.getLogger("uvicorn")
logger.propagate = 0
