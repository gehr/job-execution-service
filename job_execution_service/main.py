"""
Main module of the job-execution-service, providing the actual FastAPI / Uvicorn app.
At the moment, this provides only a few services, for posting a new job Execution, for
getting the status of a previously created Execution, and for stopping an Execution.
Everything else is handled by the job-service. The actual job-execution logic is in
the jobexecutor module.
"""
import asyncio

from fastapi import Body, FastAPI, Header, HTTPException, Path

from . import __version__
from .auth import get_receiver_auth, get_sender_auth_client
from .container_clients.docker import DockerClient
from .container_clients.kubernetes import KubernetesClient
from .database import create_db_client, get_time, migrate_db
from .jobexecutor import JobRunner
from .model import ExecStatus, Execution, PostExecution
from .mps_client import MarketplaceClient
from .singletons import logger, settings

# create App
app = FastAPI(
    title="Job Execution Service",
    version=__version__,
    redoc_url=None,
    openapi_url="/openapi.json" if not settings.disable_openapi else "",
    root_path=settings.root_path,
)
auth = get_receiver_auth()
auth_client = get_sender_auth_client()

# initialize MPS and DB client, container client, and Job Runner
mps_client = MarketplaceClient(auth_client)
db_client = create_db_client()
container_client = KubernetesClient() if settings.kubernetes_namespace else DockerClient()
job_runner = JobRunner(container_client, db_client, mps_client)


@app.on_event("startup")
async def startup_event():
    """Once when JES starts: pick up previously started jobs and connect to their containers again"""

    # show some info on startup; Note: do NOT log the full settings, in particular no credentials!
    logger.info("Job Execution Service started; Configuration:")
    logger.info("- Version %r", __version__)
    logger.info("- Database: %r @ %r", settings.database_name, settings.database_url)
    logger.info("- Using Receiver Auth: %r", settings.enable_receiver_auth)
    logger.info("- Audience: %r", settings.audience)
    logger.info("- Cleanup Interval (secs): %r", settings.time_to_clean_up_jobs_after_completion)
    logger.info("- Kubernetes Namespace: %r", settings.kubernetes_namespace)
    logger.info("- Kubernetes Config Path: %r", settings.kubernetes_config_path)
    logger.info("- Docker Host: %r", settings.docker_host)
    logger.info("- Docker GPU Driver: %r", settings.docker_gpu_driver)
    logger.info("- Docker GPU Count: %r", settings.docker_gpu_count)
    logger.info("- Docker Registries: %r", settings.registry_names)
    logger.info("- Marketplace Service: %r", settings.mps_url)
    logger.info("- Is Compute Provider: %r", settings.is_compute_provider)
    logger.info("- Log Topic: %r; Label: %r", settings.topic_label, settings.log_label)

    # database migrations
    migrate_db(db_client)

    # create update token coroutine
    asyncio.create_task(auth_client.update_token_routine())

    # initialize job-runner and container client (establish connection, queue scheduled jobs, ...)
    await container_client.initialize()
    await job_runner.on_startup()


@app.on_event("shutdown")
async def shutdown_event():
    await container_client.teardown()


@app.get("/alive", status_code=200)
async def get_service_status():
    num_scheduled = len(db_client.get_executions_with_status(ExecStatus.SCHEDULED))
    num_running = len(db_client.get_executions_with_status(ExecStatus.RUNNING))
    return {"status": "ok", "version": __version__, "num_scheduled": num_scheduled, "num_running": num_running}


@app.post(
    "/v1/executions",
    response_model=Execution,
    responses={
        200: {"description": "Job Execution created successfully"},
        400: {"description": "Malformed Job ID"},
        401: {"description": "Missing Authentication"},
        403: {"description": "Invalid Authentication"},
        404: {"description": "App not found"},
        409: {"description": "Job ID already exists"},
        412: {"description": "Mismatched Organization ID"},
        500: {"description": "Internal error when scheduling the Job"},
        502: {"description": "Could not connect to Solution Store Service"},
    },
)
async def execute_job(
    request: PostExecution = Body(..., description="The Job to be executed"),
    organization_id: str = Header(...),
    payload=auth.execute_job_depends(),
) -> Execution:
    """Get Job from job-service and schedule execution. Returns nothing, but may fail with descriptive error."""
    auth.execute_job_hook(payload=payload, request=request)

    # check if app exists immediately so this is a 404 error
    try:
        await mps_client.get_app_image_name(request.app_id, organization_id)
    except HTTPException as e:
        if e.status_code == 404:
            raise http_error(404, "App with that app_id not found in Marketplace Service") from e
        raise http_error(502, f"Could not fetch App details: {e.detail}") from e
    except Exception as e:
        raise http_error(502, f"Could not connect to Marketplace Service: {str(e) or repr(e)}") from e

    try:
        # job with same ID already executed?
        if db_client.get_execution(request.job_id) is not None:
            raise http_error(409, "A Job with the same ID has already been executed.")
    except ValueError as e:
        # malformed ID
        raise http_error(400, str(e)) from e

    try:
        execution = Execution(
            **request.model_dump(),
            status=ExecStatus.SCHEDULED,
            created_at=get_time(),
            organization_id=organization_id,
        )
        await job_runner.schedule_job(execution)
        return execution
    except Exception as e:
        raise http_error(500, str(e)) from e


@app.get(
    "/v1/executions/{job_id}",
    response_model=Execution,
    responses={
        200: {"description": "Status of given job"},
        400: {"description": "Malformed Job ID"},
        404: {"description": "Job Execution not found"},
        412: {"description": "Mismatched Organization ID"},
    },
)
async def get_status(
    job_id: str = Path(..., description="Id of the Job whose status to get"),
    organization_id: str = Header(...),
    payload=auth.get_status_depends(),
) -> Execution:
    """Get current execution status for given Job"""
    auth.get_status_hook(payload=payload, job_id=job_id)

    try:
        execution = db_client.get_execution(job_id)
        if execution is None:
            raise http_error(404, f"Job not found: {job_id}")

        # make sure only Job creator can access Execution
        if execution.organization_id != organization_id:
            raise http_error(412, "Mismatched Organization ID")

        execution.access_token = "xxxxx"  # blank out access token
        return execution
    except ValueError as e:
        # malformed ID
        raise http_error(400, str(e)) from e


@app.put(
    "/v1/executions/{job_id}/stop",
    response_model=bool,
    responses={
        200: {"description": "Job stopped (true) or already finished (false)"},
        400: {"description": "Malformed Job ID"},
        404: {"description": "Job Execution not found"},
        412: {"description": "Mismatched Organization ID"},
    },
)
async def stop_job(
    job_id: str = Path(..., description="Id of the Job to be stopped"),
    organization_id: str = Header(...),
    payload=auth.stop_job_depends(),
) -> bool:
    """Try to stop job; return true if it was cancelled or aborted, false if it was already completed"""
    auth.stop_job_hook(payload=payload, job_id=job_id)

    try:
        execution = db_client.get_execution(job_id)
        if execution is None:
            raise http_error(404, f"Job not found: {job_id}")
        if execution.organization_id != organization_id:
            raise http_error(412, "Mismatched Organization ID")

        return await job_runner.stop_job(execution)
    except ValueError as e:
        raise http_error(400, str(e)) from e


def http_error(code: int, cause: str) -> HTTPException:
    """Just a shorthand for creating a HTTPException"""
    return HTTPException(status_code=code, detail={"cause": cause})
