"""
Module for manually testing the JES for situations that can not easily be tested with unit tests.
This will either generate a matching JES Test App execution request, to be pasted into the Web UI,
or directly execute it.
"""

import json
import sys

from job_execution_service.test.common import AppTestClient, app_request

URL_LOCAL = "http://localhost:8000"
URL_CI = "https://empaia.dai-labor.de/job-execution-service-dev"
URL_NIGHTLY = "https://empaia.dai-labor.de/job-execution-service-nightly"
URL_STAGING = "https://empaia.dai-labor.de/job-execution-service-staging"
BASE_URL = URL_LOCAL


def build_request(time: int, timeout: bool, fail: bool, app_id=None):
    if timeout:
        return app_request(running_time=2 * time, fail=False, timeout=time, app_id=app_id)
    else:
        return app_request(running_time=time, fail=fail, timeout=-1, app_id=app_id)


def execute(request) -> dict:
    print("JES:", BASE_URL)
    client = AppTestClient(BASE_URL, "test")
    response = client.post_execution(request)
    print(response.status_code)
    return response.json()


def run():
    # replace with optparse or similar later...
    args = sys.argv
    fail = "fail" in args
    timeout = "timeout" in args
    exct = "exec" in args
    time = next((int(x) for x in args if x.isdigit()), 5)

    request = build_request(time, timeout, fail)
    res = execute(request) if exct else request
    print(json.dumps(res, indent=2))


if __name__ == "__main__":
    run()
