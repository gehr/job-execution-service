--- # Daemonset.yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: promtail-daemonset
  namespace: <namespace> # include the kubernetes namespace here, same as env-variable JES_KUBERNETES_NAMESPACE
spec:
  selector:
    matchLabels:
      name: promtail
  template:
    metadata:
      labels:
        name: promtail
    spec:
      serviceAccount: promtail-serviceaccount
      containers:
      - name: promtail-container
        image: grafana/promtail
        args:
        - -config.file=/etc/promtail/promtail.yaml
        - -config.expand-env=true
        env: 
        - name: 'HOSTNAME'
          valueFrom:
            fieldRef:
              fieldPath: 'spec.nodeName'
        - name: 'LOKI_USERNAME'
          valueFrom:
            secretKeyRef:
              name: promtail-secret
              key: username
        - name: 'LOKI_PASSWORD'
          valueFrom:
            secretKeyRef:
              name: promtail-secret
              key: password
        volumeMounts:
        - name: logs
          mountPath: /var/log
        - name: promtail-config
          mountPath: /etc/promtail
        - mountPath: /var/lib/docker/containers
          name: varlibdockercontainers
          readOnly: true
      volumes:
      - name: logs
        hostPath:
          path: /var/log
      - name: varlibdockercontainers
        hostPath:
          path: /var/lib/docker/containers
      - name: promtail-config
        configMap:
          name: promtail-config
--- # secrets.yaml
apiVersion: v1
kind: Secret
metadata:
  name: promtail-secret
data:
  username: USERNAME_BASE64 # promtail username base64 encoded --> `echo '<user>' | base64`
  password: PWD_BASE64 # promtail password base64 encoded --> `echo '<pw>' | base64`
--- # configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: promtail-config
data:
  promtail.yaml: |
    server:
      http_listen_port: 9080
      grpc_listen_port: 0

    clients:
# add your loki endpoint here, same as env-variable LOKI_URL
    - url: https://{YOUR_LOKI_ENDPOINT}/loki/api/v1/push
      basic_auth:
        username: ${LOKI_USERNAME}
        password: ${LOKI_PASSWORD}

    positions:
      filename: /tmp/positions.yaml
    target_config:
      sync_period: 10s
    scrape_configs:
    - job_name: pod-logs
      kubernetes_sd_configs:
        - role: pod
      pipeline_stages:
        - docker: {}
        - labelallow:
            - topic
            - log_label
            - image_name
            - job_id
            - service_url
      relabel_configs:
        - source_labels:
            - __meta_kubernetes_pod_node_name
          target_label: __host__
        - action: labelmap
          regex: __meta_kubernetes_pod_label_(.+)
        - action: replace
          replacement: $1
          separator: ':'
          source_labels:
            - image_name
            - image_tag
          target_label: image_name
        - replacement: /var/log/pods/jes_jes*$1/*.log
          separator: /
          source_labels:
            - __meta_kubernetes_pod_uid
            - __meta_kubernetes_pod_container_name
          target_label: __path__

--- # Clusterrole.yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: promtail-clusterrole
rules:
  - apiGroups: [""]
    resources:
    - nodes
    - services
    - pods
    verbs:
    - get
    - watch
    - list

--- # ServiceAccount.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: promtail-serviceaccount

--- # Rolebinding.yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: promtail-clusterrolebinding
subjects:
    - kind: ServiceAccount
      name: promtail-serviceaccount
      namespace: <namespace> # include the kubernetes namespace here, same as env-variable JES_KUBERNETES_NAMESPACE
roleRef:
    kind: ClusterRole
    name: promtail-clusterrole
    apiGroup: rbac.authorization.k8s.io