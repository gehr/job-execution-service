FROM registry.gitlab.com/empaia/integration/ci-docker-images/test-runner:0.2.8@sha256:0c20dd487c2bb040fd78991bf54f396cb1fd9ab314a7a55bee0ad4909748797d AS builder
COPY . /job-execution-service-mock
WORKDIR /job-execution-service-mock
RUN poetry build && poetry export --without-hashes -f requirements.txt > requirements.txt

FROM registry.gitlab.com/empaia/integration/ci-docker-images/python-base:0.2.10@sha256:439442ef70fba054b0edd9c96508574cb297ebdc4fdb4904428bebf4432ba471
USER root
COPY --from=builder /job-execution-service-mock/requirements.txt /artifacts
RUN pip install -r /artifacts/requirements.txt
COPY --from=builder /job-execution-service-mock/dist /artifacts
COPY ./app_requirements.json /artifacts/
RUN pip install /artifacts/*.whl
EXPOSE 8000
ENTRYPOINT [ "uvicorn", "job_execution_service.main:app", "--host", "0.0.0.0", "--port", "8000" ]
